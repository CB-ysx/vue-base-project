# vue-base-project

> 一个vue项目的基础框架

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## 使用

* axios
* flex.css
* moment
* normalize.css
* nprogress
* element-ui
* qs
* vuex
* vuex-router-sync
* web-storage-cache